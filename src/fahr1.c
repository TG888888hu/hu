#include <stdio.h>
/*печать таблицы по фаренгейту*/
int main(){
float fahr, Celsius;
int lower, upper, step;
lower=0;
upper=300;
step=20;
fahr=lower;
while (fahr<=upper){
    Celsius=5.0*(fahr-32.0)/9.0;
    printf("%3.0f%6.1f\n", fahr, Celsius);
    fahr=fahr+step;
}
}
